package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.Cake;
import de.gymnasium_oberstadt.gameimpossible.entity.Carrot;
import de.gymnasium_oberstadt.gameimpossible.entity.Door;
import de.gymnasium_oberstadt.gameimpossible.entity.Player;

public class OpeningTerrain extends Terrain {

    private static OpeningTerrain instance = null;

    private OpeningTerrain() {
        super("Background_OpeningTerrain.jpg");
    }

    public static OpeningTerrain getInstance() {
        if (instance == null) {
            instance = new OpeningTerrain();
        }
        return instance;
    }

    @Override
    public Terrain getNextTerrain() {
        return null;
    }

    @Override
    public void loadLevel1() {
        addObject(Player.getInstance(), getWidth() / 2, getHeight() / 2);

        addObject(new Door(SpawnTerrain.getInstance(), false), 150, 200);
        addObject(new Door(ForestTerrain.getInstance(), true), 460, 200);
        addObject(new Door(HellTerrain.getInstance(), true), 760, 200);
        addObject(new Door(SupernaturalBeingsTerrain.getInstance(), true), 1085, 200);
    }

    @Override
    public void loadLevel2() {

    }

    @Override
    public void loadLevel3() {

    }
}

package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.*;
import de.gymnasium_oberstadt.gameimpossible.helper.CraftRecipe;
import de.gymnasium_oberstadt.gameimpossible.quest.KillQuest;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestManager;
import de.gymnasium_oberstadt.gameimpossible.tool.Text;

public class SpawnTerrain extends Terrain {

    private static SpawnTerrain instance = null;

    private SpawnTerrain() {
        super("Background.png");
    }

    public static SpawnTerrain getInstance() {
        if (instance == null) {
            instance = new SpawnTerrain();
        }
        return instance;
    }

    @Override
    public Terrain getNextTerrain() {
        return ForestTerrain.getInstance();
    }

    @Override
    public void loadLevel1() {
        // TODO: load and save Player from and to file
        addObject(Player.getInstance(), 30, 30);

        // recipes have to be added after Player is in a Terrain
        addRecipes();
        addObject(new Trap(), 120, 30);
        addObject(new Wood(), 80, 150);
        addObject(new Wood(), 100, 200);
        addObject(new Wood(), 120, 150);
        addObject(new Wood(), 140, 200);
        addObject(new Tree(), 160, 240);
        addObject(new Thicket(), 500, 500);

        addObject(new Trap(), 133, 111);
        addObject(new Tree(), 244, 344);
        addObject(new Tree(), 555, 551);
        addObject(new Tree(), 999, 430);
        addObject(new Tree(), 333, 431);
        addObject(new Tree(), 332, 432);
        addObject(new Tree(), 331, 433);
        addObject(new Tree(), 330, 434);
        addObject(new Trap(), 133, 111);
        addObject(new Tree(), 244, 344);
        addObject(new Tree(), 555, 551);
        addObject(new Tree(), 999, 430);
        addObject(new Tree(), 333, 431);
        addObject(new Tree(), 332, 432);
        addObject(new Tree(), 331, 433);
        addObject(new Tree(), 331, 435);

        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,100);
        }
        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,125);
        }
        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,150);
        }
        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,175);
        }
        for (int i = 0; i < 9500; i= i+30) {
            addObject(new Tree(), 800+i,200);
        }
        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,50);
        }
        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,25);
        }
        for (int i = 0; i < 9500; i = i+30) {
            addObject(new Tree(), 800+i,75);
        }

        for(int i = 0; i < 300; i = i +30){
            addObject(new Wood(), 50,300+i);
        }

        addObject(new Trap(), 333, 111);
        addObject(new Trap(), 133, 711);
        addObject(new Trap(), 566, 444);

        addObject(new Jewel(), 433, 333);
        addObject(new Jewel(), 665, 555);
        addObject(new Jewel(), 777, 666);

        for(int i = 0; i < 300; i = i +30){
            addObject(new Thicket(), 85,300+i);
        }
        for(int i = 0; i < 300; i = i +30){
            addObject(new Wood(), 120,300+i);
        }
        for(int i = 0; i < 300; i = i +30){
            addObject(new Thicket(), 165,300+i);
        }



        Bear bear = new Bear();
        addObject(bear, 400, 400);
        KillQuest killQuest = new KillQuest(bear);
        Door door = new Door(OpeningTerrain.getInstance(), true, killQuest);
        addObject(door, 1000, 500);

        addObject(new Key(door), 900, 700);

        addObject(new Sword(Material.SILVER), 1400, 500);
        addObject(new Carrot(), 1000, 600);

        addObject(new Potion(), 800, 600);

        addObject(new Text("Wie du siehst, besitzt du noch keine Waffen. Dein Ziel in dieser Welt ist es, eine Nahkampfwaffe zu erlangen.\n " +
                "Dadurch musst du in dieser Welt drei Level durchleben, dann öffnet sich die nächste Tür mit der nächsten Welt für dich.\n" +
                "Aber Achtung, es warten in der Welt Gegner auf dich.\n " +
                "Suche Items im Wald durch Zerstören der Objekte. Mache dich auf den Weg zum Amboss um Sachen herzustellen."), getWidth() - 460, getHeight() - 60);

        QuestManager questManager = new QuestManager();
        questManager.registerQuests(killQuest);
        Player.getInstance().setQuestManager(questManager);

        addObject(new Cake(),500,500);

    }

    @Override
    public void loadLevel2() {

    }

    @Override
    public void loadLevel3() {

    }

    public void addRecipes() {
        addRecipe(new CraftRecipe(new String[]{"Wood", "Wood"}, new StaticEntity[]{new Sword(Material.STEEL)}));
        // TODO: add more
    }
}
package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.*;
import de.gymnasium_oberstadt.gameimpossible.helper.CraftRecipe;
import de.gymnasium_oberstadt.gameimpossible.quest.KillQuest;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestManager;

import java.util.List;

public class HellTerrain extends Terrain {

    private static HellTerrain instance = null;

    private HellTerrain() {
        super("Background_Hell.jpg");
    }

    public static HellTerrain getInstance() {
        if (instance == null) {
            instance = new HellTerrain();
        }
        return instance;
    }

    @Override
    public Terrain getNextTerrain() {
        return SupernaturalBeingsTerrain.getInstance();
    }

    @Override
    public String getRockFileName() {
        return "Rock_Hell.png";
    }

    @Override
    public float getMaxPlayerLife() {
        return 14;
    }

    @Override
    public void loadLevel1() {
        addObject(Player.getInstance(), 30, 30);

        addRecipes();
        addObject(new Wood(),100,100);
        addObject(new Rope(),100,100);

        Witch witch = new Witch();
        addObject(witch, 900, 200);
        addObject(new Witch(6), 900, 500);
        addObject(new Witch(2), 900, 478);
        addObject(new Witch(7), 900, 297);
        addObject(new Witch(4), 900, 706);

        KillQuest killQuestWitch = new KillQuest(witch);

        QuestManager questManager = new QuestManager();
        questManager.registerQuests(killQuestWitch);
        Player.getInstance().setQuestManager(questManager);

        Door door = new Door(OpeningTerrain.getInstance(), true,killQuestWitch);
        addObject(door, 300, 300);
        addObject(new Key(door), 500, 500);

        addObject(new Trap(), 333,444);
        addObject(new Trap(), 444,555);
        addObject(new Trap(), 10,555);

        addObject(new Trap(), 333,444);

        addObject(new Wood(), 333,222);

        addObject(new Trap(), 333,444);

        addObject(new Rope(), 333,444);
    }

    @Override
    public void loadLevel2() {

    }

    @Override
    public void loadLevel3() {

    }

    public void addRecipes() {
        addRecipe(new CraftRecipe(new String[]{"Wood", "Wood"}, new StaticEntity[]{new Sword(Material.FERUM)}));
        addRecipe(new CraftRecipe(new String[]{"Carrot", "Carrot"}, new StaticEntity[]{new Cake()}));
        addRecipe(new CraftRecipe(new String[]{"Wood", "Rope"},new StaticEntity[]{new Bow(Material.STEEL,Player.getInstance())}));
    }
}
package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.Arrow;
import de.gymnasium_oberstadt.gameimpossible.entity.Entity;
import de.gymnasium_oberstadt.gameimpossible.entity.Player;
import de.gymnasium_oberstadt.gameimpossible.helper.CraftRecipe;
import de.gymnasium_oberstadt.gameimpossible.helper.Texts;
import greenfoot.Greenfoot;
import greenfoot.World;

import java.util.LinkedList;
import java.util.List;

public abstract class Terrain extends World {
    private List<CraftRecipe> recipes = new LinkedList<>();

    public static final int IMAGE_SIZE = 30;

    public static final int EQUIP_IMAGE_SIZE = 20;

    public Terrain(String fileName) {
        super(1200, 800, 1);
        setBackground(Texts.PATH + "images/terrains/" + fileName);
        setPaintOrder(Player.class);
    }

    @Override
    public void act() {
        List<Arrow> arrows = getObjects(Arrow.class);
        for (int i = arrows.size() - 1; i >= 0; i--) {
            Arrow arrow = arrows.get(i);
            boolean keepInWorld = arrow.update();
            if (!keepInWorld) {
                arrow.removeFromTerrain();
            }
        }
    }

    public void clearTerrain() {
        List<Entity> objects = getObjects(Entity.class);
        for (Entity obj : objects) {
            obj.removeFromTerrain();
        }
    }

    public static void setTerrain(Terrain terrain) {
        Greenfoot.setWorld(terrain);
        Player.getInstance().setTerrain(terrain);
        for (Entity inventoryItem : Player.getInstance().getInventoryItems()) {
            if (inventoryItem != null) {
                inventoryItem.setTerrain(terrain);
            }
        }
        Entity equipedItem = Player.getInstance().getEquipedItem();
        if (equipedItem != null) {
            equipedItem.setTerrain(terrain);
        }
    }

    public abstract Terrain getNextTerrain();

    public abstract void loadLevel1();

    public abstract void loadLevel2();

    public abstract void loadLevel3();

    public void addRecipe(CraftRecipe recipe) {
        recipes.add(recipe);
    }

    public List<CraftRecipe> getRecipes() {
        return recipes;
    }

    public String getWitchFileName() {
        return "Witch.png";
    }

    public String getAnvilFileName() {
        return "Anvil.png";
    }

    public String getCarrotFileName() {
        return "Carrot.png";
    }

    public String getCakeFileName() {
        return "Cake.png";
    }

    public String getOpenDoorFileName() {
        return "Door_open.png";
    }

    public String getClosedDoorFileName() {
        return "Door_closed.png";
    }

    public String getLockedDoorFileName() {
        return "Door_locked.png";
    }

    public String getKeyFileName() {
        return "Key.png";
    }

    public String getRockFileName() {
        return "Rock.png";
    }

    public String getJewelFileName() {
        return "Jewel.png";
    }


    public String getWoodFileName() {
        return "Wood.png";
    }

    public String getMagicballsFileName() {
        return "Magicballs.png";
    }

    public String getShovelFileName() {
        return "Shovel.png";
    }

    public String getSwordFileName() {
        return "Sword.png";
    }

    public String getBowFileName() {
        return "Bow.png";
    }

    public String getThicketFileName() {
        return "Thicket.png";
    }

    public String getTrapFileName() {
        return "Trap.png";
    }

    public String getTreeFileName() {
        return "Tree.png";
    }

    public String getBearFileName() {
        return "Bear.png";
    }

    public String getWolfFileName() {
        return "Wolf.png";
    }


    public String getPotionFileName() {
        return "Potion.png";
    }

    public String getRopeFileName() {
        return "Rope.png";
    }


    public float getMaxPlayerLife() {
        return 10.0f;
    }

    public float getMaxEnemyLife() {
        return 10.0f;
    }

    public float getMaxBearLife() {
        return 5.0f;
    }

    public float getMaxWolfLife() {
        return 6.0f;
    }

    public float getMaxWitchLife() {
        return 7.0f;
    }
}


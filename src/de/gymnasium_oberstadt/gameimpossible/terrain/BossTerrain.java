package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.Player;
import de.gymnasium_oberstadt.gameimpossible.entity.Wolf;
import de.gymnasium_oberstadt.gameimpossible.quest.KillQuest;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestManager;

public class BossTerrain extends Terrain {

    private static BossTerrain instance = null;

    private BossTerrain() {
        super("Background_BossTerrain.jpg");
    }

    public static BossTerrain getInstance() {
        if (instance == null) {
            instance = new BossTerrain();
        }
        return instance;
    }

    @Override
    public Terrain getNextTerrain() {
        return null;
    }

    @Override
    public String getWolfFileName() {
        return "Monster_Schaede.png";
    }

    @Override
    public float getMaxPlayerLife() {
        return 18;
    }

    @Override
    public void loadLevel1() {
        addObject(Player.getInstance(), 30, 30);
        Wolf wolf = new Wolf();
        addObject(wolf, 600, 400);

        KillQuest killQuest = new KillQuest(wolf);

        QuestManager questManager = new QuestManager();
        questManager.registerQuests(killQuest);
        Player.getInstance().setQuestManager(questManager);
    }

    @Override
    public void loadLevel2() {

    }

    @Override
    public void loadLevel3() {

    }
}

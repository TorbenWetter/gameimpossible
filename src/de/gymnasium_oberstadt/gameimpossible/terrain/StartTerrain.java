package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.helper.Texts;
import greenfoot.GreenfootSound;

public class StartTerrain extends Terrain {

    private static GreenfootSound music = new GreenfootSound(Texts.PATH + "sounds/MissionImpossible.wav");

    public StartTerrain() { // can't be a a Singleton because it's instantiated by Greenfoot
        super("StartScreen.jpg");
        music.playLoop();
    }

    @Override
    public void act() {
        setTerrain(Intro.getInstance());
        Intro.getInstance().loadLevel1();
    }

    public static void stopMusic() {
        music.stop();
    }

    @Override
    public Terrain getNextTerrain() {
        return null;
    }

    @Override
    public void loadLevel1() {
        // do nothing
    }

    @Override
    public void loadLevel2() {
        // do nothing
    }

    @Override
    public void loadLevel3() {
        // do nothing
    }
}

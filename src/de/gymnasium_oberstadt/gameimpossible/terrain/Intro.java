package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.tool.Text;
import greenfoot.Greenfoot;

import java.awt.*;

public class Intro extends Terrain {

    private static Intro instance;

    private Intro() {
        super("Background_Intro.jpg");
    }

    public static Intro getInstance() {
        if (instance == null) {
            instance = new Intro();
        }
        return instance;
    }

    @Override
    public void act() {
        if (Greenfoot.isKeyDown("+")) {
            loadLevel2();
        }
    }

    @Override
    public Terrain getNextTerrain() {
        return null;
    }

    @Override
    public void loadLevel1() {
        addObject(new Text("Willkommen du tapferer Held.\n" +
                "Hier spricht der Magier Merlin, ich habe dich gerufen damit du uns hilfst.\n" + "Unsere Welten sind in Gefahr.\n" + "Der Bösewicht Herr Schaede treibt mit seinen bösen Kreaturen sein Unwesen in unseren Welten. Unsere Heimat ist kurz davor zerstört zu werden.\n" +
                "Nur du kannst uns und unsere Welten retten.\n" + "Mache dich auf den Weg und besiege Herrn Schaede. Bereite dich auf den Endkampf vor.\n" + "Gehe durch die Türen und löse die Rätsel um weiter zu kommen."), getWidth() / 2, getHeight() / 2);
        addObject(new Text("+", 50, Color.BLACK, Color.CYAN), getWidth() - 30, getHeight() - 30);
    }

    @Override
    public void loadLevel2() {
        setTerrain(OpeningTerrain.getInstance());
        OpeningTerrain.getInstance().loadLevel1();

    }

    @Override
    public void loadLevel3() {

    }
}

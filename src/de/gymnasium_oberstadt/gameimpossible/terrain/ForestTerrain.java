package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.*;
import de.gymnasium_oberstadt.gameimpossible.helper.CraftRecipe;
import de.gymnasium_oberstadt.gameimpossible.quest.KillQuest;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestManager;

public class ForestTerrain extends Terrain {

    private static ForestTerrain instance = null;

    private ForestTerrain() {
        super("Background_Forest.jpg");
    }

    public static ForestTerrain getInstance() {
        if (instance == null) {
            instance = new ForestTerrain();
        }
        return instance;
    }

    @Override
    public Terrain getNextTerrain() {
        return HellTerrain.getInstance();
    }

    @Override
    public String getRockFileName() {
        return "Rock_Forest.png";
    }

    public float getMaxPlayerLife() {
        return 12;
    }


    @Override
    public void loadLevel1() {
        addObject(Player.getInstance(), 30, 30);

        addRecipes();
        Wolf wolf = new Wolf();
        addObject(wolf, 1000, 600);

        KillQuest killQuestWolf = new KillQuest(wolf);

        QuestManager questManager = new QuestManager();
        questManager.registerQuests(killQuestWolf);
        Player.getInstance().setQuestManager(questManager);

        Door door = new Door(OpeningTerrain.getInstance(), true, killQuestWolf);

        addObject(door, 1000, 500);
        addObject(new Key(door), 500, 500);
        addObject(new Wood(), 40, 30);

        addObject(new Wood(), 30, 30);
        addObject(new Rock(), 60, 30);
        addObject(new Rock(), 60, 60);
        addObject(new Rock(), 120, 30);
        addObject(new Rock(), 1200, 45);
        addObject(new Rock(), 210, 30);
        addObject(new Rock(), 180, 78);
        addObject(new Rock(), 1100, 30);
        addObject(new Rock(), 900, 22);
        addObject(new Rock(), 347, 30);
        addObject(new Rock(), 567, 40);
        addObject(new Rock(), 65, 30);
        addObject(new Rock(), 88, 55);
        addObject(new Rock(), 258, 12);
        for (int i = 800; i < 1200; i = i + 100) {
            for (int k = 350; k < 700; k = k + 50) {
                addObject(new Tree(), i, k);
            }
        }
        addObject(new Jewel(), 300, 400);
        addObject(new Jewel(), 300, 500);

        addObject(new Bow(Material.STEEL, Player.getInstance()), 600, 600);


        addObject(new Carrot(), 300,300);
        addObject(new Carrot(), 10,30);
        addRecipes();
    }

    @Override
    public void loadLevel2() {
        addObject(Player.getInstance(), 30, 30);
        addObject(new Wood(), 60, 60);
    }

    @Override
    public void loadLevel3() {
        addObject(Player.getInstance(), 30, 30);
        addObject(new Wood(), 90, 90);
    }

    public void addRecipes() {
        addRecipe(new CraftRecipe(new String[]{"Wood", "Wood"}, new StaticEntity[]{new Sword(Material.FERUM)}));
        addRecipe(new CraftRecipe(new String[]{"Carrot", "Carrot"}, new StaticEntity[]{new Cake()}));
    }
}
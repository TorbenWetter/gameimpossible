package de.gymnasium_oberstadt.gameimpossible.terrain;

import de.gymnasium_oberstadt.gameimpossible.entity.*;
import de.gymnasium_oberstadt.gameimpossible.helper.CraftRecipe;
import de.gymnasium_oberstadt.gameimpossible.quest.CollectQuest;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestManager;

public class SupernaturalBeingsTerrain extends Terrain {

    private static SupernaturalBeingsTerrain instance;

    private SupernaturalBeingsTerrain() {
        super("Backgroun_SupernaturalBeing.jpg");
    }

    public static SupernaturalBeingsTerrain getInstance() {
        if (instance == null) {
            instance = new SupernaturalBeingsTerrain();
        }
        return instance;
    }

    @Override
    public String getRockFileName() {
        return "Rock_SNB.png";
    }

    @Override
    public float getMaxPlayerLife() {
        return 16;
    }

    @Override
    public Terrain getNextTerrain() {
        return null;
    }


    @Override
    public void loadLevel1() {
        addObject(Player.getInstance(), 30, 30);

        addRecipes();


        Jewel jewel1 = new Jewel();
        Jewel jewel2 = new Jewel();
        Jewel jewel3 = new Jewel();

        addObject(jewel1, 700, 500);
        addObject(jewel2, 700, 600);
        addObject(jewel3, 800, 500);

        CollectQuest collectQuest1 = new CollectQuest(jewel1);
        CollectQuest collectQuest2 = new CollectQuest(jewel2);
        CollectQuest collectQuest3 = new CollectQuest(jewel3);

        QuestManager questManager = new QuestManager();
        //quest: you need to collect all jewels

        questManager.registerQuests(collectQuest1,collectQuest2,collectQuest3);


        Player.getInstance().setQuestManager(questManager);


        Door door = new Door(BossTerrain.getInstance(), true,collectQuest1);
        addObject(door, 1000, 500);
        addObject(new Key(door), 500, 500);

        addObject(new Jewel(), 111,555);
        addObject(new Jewel(), 10,20);
    }

    @Override
    public void loadLevel2() {

    }

    @Override
    public void loadLevel3() {

    }

    public void addRecipes() {
        addRecipe(new CraftRecipe(new String[]{"Wood", "Wood"}, new StaticEntity[]{new Sword(Material.FERUM)}));
        addRecipe(new CraftRecipe(new String[]{"Carrot", "Carrot"}, new StaticEntity[]{new Cake()}));
        addRecipe(new CraftRecipe(new String[]{"Wood", "Rope"},new StaticEntity[]{new Bow(Material.STEEL,Player.getInstance())}));
        addRecipe(new CraftRecipe(new String[]{"Jewel", "Jewel"}, new StaticEntity[]{new Potion()}));
    }
}

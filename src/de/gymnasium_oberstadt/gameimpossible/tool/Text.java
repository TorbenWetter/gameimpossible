package de.gymnasium_oberstadt.gameimpossible.tool;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

import java.awt.*;

public class Text extends Actor {
    public Text(String string) {
        setImage(new GreenfootImage(string, 20, Color.BLACK, Color.WHITE));
    }

    public Text(String string, int size, Color colorFont, Color colorGround) {
        setImage(new GreenfootImage(string, size, colorFont, colorGround));
    }
}

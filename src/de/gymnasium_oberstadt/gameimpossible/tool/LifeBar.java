package de.gymnasium_oberstadt.gameimpossible.tool;

import de.gymnasium_oberstadt.gameimpossible.entity.StaticEntity;
import de.gymnasium_oberstadt.gameimpossible.property.Blocking;
import de.gymnasium_oberstadt.gameimpossible.terrain.Terrain;
import greenfoot.GreenfootImage;

import java.awt.*;

import static java.lang.Math.round;

public class LifeBar extends StaticEntity implements Blocking {

    private float maxLife;

    public LifeBar(float maxLife) {
        super(null);
        this.maxLife = maxLife;
    }

    public void update(float life) {
        setImage(new GreenfootImage(round(maxLife * Terrain.IMAGE_SIZE), Terrain.IMAGE_SIZE));

        drawFilling(life, 1, Color.GREEN);
        drawBorder(maxLife, 1, Color.BLACK);
    }

    public void drawFilling(float width, int height, Color color) {
        getImage().setColor(color);
        getImage().fillRect(0, 0, round(width * Terrain.IMAGE_SIZE), height * Terrain.IMAGE_SIZE);
    }

    public void drawBorder(float width, int height, Color color) {
        getImage().setColor(color);
        getImage().drawRect(0, 0, round(width * Terrain.IMAGE_SIZE) - 1, height * Terrain.IMAGE_SIZE - 1);
    }
}

package de.gymnasium_oberstadt.gameimpossible.tool;

import de.gymnasium_oberstadt.gameimpossible.entity.Entity;
import de.gymnasium_oberstadt.gameimpossible.entity.StaticEntity;
import de.gymnasium_oberstadt.gameimpossible.helper.Texts;
import de.gymnasium_oberstadt.gameimpossible.terrain.Terrain;
import greenfoot.GreenfootImage;
import greenfoot.World;

import java.awt.*;

public class Inventory extends StaticEntity {

    private int capacity;

    private Entity[] items;
    private GreenfootImage[] itemImages;

    private GreenfootImage emptySlot;

    private int selected;

    public Inventory(int capacity) {
        super(null);

        this.capacity = capacity;

        this.emptySlot = new GreenfootImage(Texts.PATH + "images/tiles/EmptySlot.png");
        this.emptySlot.scale(Terrain.IMAGE_SIZE, Terrain.IMAGE_SIZE);
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        if (items == null) {
            setLinkedItems(new Entity[capacity]);
        }
    }

    @Override
    public void act() {
        setImage(new GreenfootImage(this.capacity * Terrain.IMAGE_SIZE, Terrain.IMAGE_SIZE));
        for (int i = 0; i < items.length; i++) { // loop through all slots to set new images if necessary
            Entity item = items[i];
            if (item == null && itemImages[i] != emptySlot) { // if item is null but item image not yet emptySlot
                getImage().drawImage(emptySlot, i * Terrain.IMAGE_SIZE, 0);
                itemImages[i] = emptySlot;
            } else if (item != null && itemImages[i] != item.getImage()) { // if item is not null but the slot image is not yet the item's image
                getImage().drawImage(emptySlot, i * Terrain.IMAGE_SIZE, 0);
                GreenfootImage newImage = item.getImage();
                newImage.scale(Terrain.IMAGE_SIZE, Terrain.IMAGE_SIZE);
                getImage().drawImage(newImage, i * Terrain.IMAGE_SIZE, 0);
                itemImages[i] = newImage;
            } else {
                break;
            }
            if (i == this.selected) {
                Color cyan = Color.CYAN;
                int transparency = 64;
                getImage().setColor(new Color(cyan.getRed(), cyan.getGreen(), cyan.getBlue(), transparency));
                getImage().fillRect(i * Terrain.IMAGE_SIZE + 1, 1, Terrain.IMAGE_SIZE - 2, Terrain.IMAGE_SIZE - 2);
                getImage().setColor(cyan);
                getImage().drawRect(i * Terrain.IMAGE_SIZE, 0, Terrain.IMAGE_SIZE - 1, Terrain.IMAGE_SIZE - 1);
            }
        }
    }

    public void setLinkedItems(Entity[] inventory) {
        if (inventory.length != this.capacity) {
            System.out.println("Capacities are different, ERROR!");
            return;
        }

        this.items = inventory;
        itemImages = new GreenfootImage[capacity];
    }

    public int getSelected() {
        return this.selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}

package de.gymnasium_oberstadt.gameimpossible.quest;

import de.gymnasium_oberstadt.gameimpossible.entity.MovingEntity;
import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class QuestManager {

    private Quest[] quests;

    public void registerQuests(Quest... quests) {
        this.quests = quests;
    }

    public void triggerEvent(QuestType type, Object affected) {
        for (Quest quest : quests) {
            if (!quest.isDone()) {
                if (type == QuestType.KILL && quest instanceof KillQuest) {
                    MovingEntity killed = (MovingEntity) affected;
                    KillQuest killQuest = (KillQuest) quest;
                    if (killQuest.getToBeKilled() == killed) {
                        killQuest.setDone();
                    }
                } else if (type == QuestType.COLLECT && quest instanceof CollectQuest) {
                    Pickable collected = (Pickable) affected;
                    CollectQuest collectQuest = (CollectQuest) quest;
                    if (collectQuest.getToBeCollected() == collected) {
                        collectQuest.setDone();
                    }
                }
            }
        }
    }
}

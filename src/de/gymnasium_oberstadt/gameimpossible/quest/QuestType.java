package de.gymnasium_oberstadt.gameimpossible.quest;

public enum QuestType {
    KILL, COLLECT
}

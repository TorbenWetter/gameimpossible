package de.gymnasium_oberstadt.gameimpossible.quest;

public abstract class Quest {
    private boolean done = false;

    public boolean isDone() {
        return done;
    }

    public void setDone() {
        done = true;
    }
}

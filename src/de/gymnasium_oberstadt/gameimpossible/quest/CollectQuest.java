package de.gymnasium_oberstadt.gameimpossible.quest;

import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class CollectQuest extends Quest {

    private Pickable toBeCollected;

    public CollectQuest(Pickable toBeCollected) {
        this.toBeCollected = toBeCollected;
    }

    public Pickable getToBeCollected() {
        return toBeCollected;
    }
}

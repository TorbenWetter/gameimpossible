package de.gymnasium_oberstadt.gameimpossible.quest;

import de.gymnasium_oberstadt.gameimpossible.entity.MovingEntity;

public class KillQuest extends Quest {

    private MovingEntity toBeKilled;

    public KillQuest(MovingEntity toBeKilled) {
        this.toBeKilled = toBeKilled;
    }

    public MovingEntity getToBeKilled() {
        return toBeKilled;
    }
}

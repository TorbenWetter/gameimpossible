package de.gymnasium_oberstadt.gameimpossible.helper;

import de.gymnasium_oberstadt.gameimpossible.entity.Entity;
import de.gymnasium_oberstadt.gameimpossible.entity.MovingEntity;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;

public class Timer {

    private Triggerable toTrigger;
    private TriggerEvent triggerEvent;
    private long startTime;
    private float time;
    private boolean isRunning;
    private MovingEntity movingEntity;

    public Timer(Triggerable toTrigger, TriggerEvent triggerEvent, MovingEntity movingEntity) {
        this.toTrigger = toTrigger;
        this.triggerEvent = triggerEvent;
        this.startTime = System.currentTimeMillis();
        this.time = triggerEvent.getTime();
        this.isRunning = true;
        this.movingEntity = movingEntity;
    }

    public void update() {
        if (this.isRunning && System.currentTimeMillis() > this.startTime + Math.round(this.time * 1000)) {
            this.isRunning = false;
            this.toTrigger.trigger(this.movingEntity, this.triggerEvent);
        }
    }

    public boolean isRunning() {
        return this.isRunning;
    }
}

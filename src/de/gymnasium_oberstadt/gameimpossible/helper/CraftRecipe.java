package de.gymnasium_oberstadt.gameimpossible.helper;

import de.gymnasium_oberstadt.gameimpossible.entity.StaticEntity;

public class CraftRecipe {

    private String[] from;
    private StaticEntity[] to;

    public CraftRecipe(String[] from, StaticEntity[] to) {
        this.from = from;
        this.to = to;
    }

    public String[] getClassNames() {
        return from;
    }

    public StaticEntity[] getEntities() {
        return to;
    }
}

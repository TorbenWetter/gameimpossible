package de.gymnasium_oberstadt.gameimpossible.helper;

public enum Direction {

    LEFT(180),
    RIGHT(0),
    DOWN(90),
    UP(270);

    private int value;

    private Direction(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static Direction fromValue(int value) {
        for (Direction direction : values()) {
            if (direction.getValue() == value) {
                return direction;
            }
        }
        return null;
    }
}

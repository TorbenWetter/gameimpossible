package de.gymnasium_oberstadt.gameimpossible.helper;

public enum TriggerEvent {

    READY_FOR_ATTACK(1.0f),
    RELEASE(2.0f),
    SET_VISIBLE(0.5f),
    REACTIVATE(4.0f),
    DEACTIVATE(2.0f), //if there is no trapping but a time, when the item does not "work"
    READY_SPAWNING(4.0f), //witch spawns magicballs
    DELETE_STONES(3.0f),
    STOP_ACCELERATION(4.0f);

    private float time;

    TriggerEvent(float time) {
        this.time = time;
    }

    public float getTime() {
        return this.time;
    }
}

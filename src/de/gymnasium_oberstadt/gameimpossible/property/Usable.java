package de.gymnasium_oberstadt.gameimpossible.property;

import de.gymnasium_oberstadt.gameimpossible.entity.Player;

public interface Usable {

    public void use(Player player);
}

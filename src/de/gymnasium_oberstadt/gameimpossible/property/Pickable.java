package de.gymnasium_oberstadt.gameimpossible.property;

import de.gymnasium_oberstadt.gameimpossible.entity.Player;

public interface Pickable {

    public void pick(Player player);
}

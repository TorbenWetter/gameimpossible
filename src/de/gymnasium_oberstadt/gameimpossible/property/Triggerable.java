package de.gymnasium_oberstadt.gameimpossible.property;

import de.gymnasium_oberstadt.gameimpossible.entity.MovingEntity;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;

public interface Triggerable {

    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent);
}

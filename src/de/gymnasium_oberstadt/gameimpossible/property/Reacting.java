package de.gymnasium_oberstadt.gameimpossible.property;

import de.gymnasium_oberstadt.gameimpossible.entity.MovingEntity;

public interface Reacting {

    public void react(MovingEntity movingEntity);
}

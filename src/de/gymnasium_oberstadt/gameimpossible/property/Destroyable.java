package de.gymnasium_oberstadt.gameimpossible.property;

import de.gymnasium_oberstadt.gameimpossible.entity.Player;

public interface Destroyable {
    public void destroy(Player p);
}


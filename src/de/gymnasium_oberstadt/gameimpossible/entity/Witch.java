package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Direction;
import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Reacting;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;
import greenfoot.World;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Witch extends Enemy implements Reacting, Triggerable {
    private boolean isActive;

    private boolean spawningActivated;

    private List<Timer> timers = new LinkedList<>();

    private float damage;

    private int speed;

    private List<MagicBalls> magicBalls = new ArrayList<>(5);

    public Witch() {
        super(Player.getInstance().getTerrain().getWitchFileName());
        this.isActive = true;
        spawningActivated = true;
        damage = getDamage();
        speed = getSpeed();
        turn(Direction.LEFT);
    }

    public Witch(int speed) {
        super(Player.getInstance().getTerrain().getWitchFileName());
        this.isActive = true;
        damage = getDamage();
        this.speed = speed;
        turn(Direction.LEFT);
        magicBalls = fillMagic();
        spawningActivated = true;
    }

    public List<MagicBalls> fillMagic() {
        for (int i = 0; i < 10; i++) {
            magicBalls.add(new MagicBalls(this));
        }
        return magicBalls;
    }

    public void act() {
        super.act();

        if (isInWorld()) {
            moveVertically(speed);
            throwThings();
        }
    }

    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.READY_SPAWNING) {
            this.spawningActivated = true;
        }
        if (triggerEvent == TriggerEvent.READY_FOR_ATTACK) {
            this.isActive = true;
        }
    }

    @Override
    public float getDamage() {
        return 2.0f;
    }


    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        float maxLife = Player.getInstance().getTerrain().getMaxWitchLife();
        setLife(maxLife);
    }

    public int getSpeed() {
        return 8;
    }


    @Override
    public void react(MovingEntity movingEntity) {
        List<MovingEntity> movingEntities = getIntersectingObjects(MovingEntity.class); //if Player touches witch he gets hurt
        for (MovingEntity entity : movingEntities) {
            if (entity instanceof Player) {
                if (this.isActive) {
                    this.isActive = false;
                    movingEntity.applyDamage(damage);
                    timers.add(new Timer(this, TriggerEvent.READY_FOR_ATTACK, movingEntity));
                    break;
                }
            }
        }
    }

    public void throwThings() {
        if (Player.getInstance().isInWorld()) {
            if (seeEntity(Player.getInstance(), getTerrain().getCellSize()*2)) {
                for (int i = 0; i < magicBalls.size(); i++) {
                    if (magicBalls.get(i) != null) {
                        getTerrain().addObject(magicBalls.get(i), getX(), getY());
                        timers.add(new Timer(this, TriggerEvent.READY_SPAWNING, magicBalls.get(i)));
                        this.spawningActivated = false;
                        magicBalls.remove(i);
                        break;
                    }
                }
            }
        }
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Equipable;
import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Sword extends Weapon implements Pickable, Equipable {

    public Sword(Material material) {
        super(Player.getInstance().getTerrain().getSwordFileName(), material);
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }
}

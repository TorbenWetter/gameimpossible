package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Reacting;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;
import greenfoot.World;

import java.util.LinkedList;
import java.util.List;

public class MagicBalls extends Enemy implements Triggerable, Reacting {
    private boolean isAktivated; //it needs to be another attribute otherwise there will be an intersection with the methods if witch which in case will run parallel

    private List<Timer> timers = new LinkedList<>();

    private Entity owner;

    private float damage = getDamage();

    private int speed;

    public MagicBalls(Entity owner) {
        super(Player.getInstance().getTerrain().getMagicballsFileName());
        this.owner = owner;
        isAktivated = true;
        this.speed = getSpeed();
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        float maxLife = Player.getInstance().getTerrain().getMaxWitchLife();
        setLife(maxLife);
    }

    @Override
    public void act() {
        if (isAlive()) {
            super.act();

            if (isInWorld()) {
                react(Player.getInstance());
                rotate();
            }
            timers.add(new Timer(this, TriggerEvent.DELETE_STONES, Player.getInstance()));
        }
    }

    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.READY_FOR_ATTACK) {
            this.isAktivated = true;
        } else if (triggerEvent == TriggerEvent.DELETE_STONES) {
            removeFromTerrain();
        }
    }

    @Override
    public float getDamage() {
        return 1.0f;
    }

    public void rotate() {
        if (isAlive() && isInWorld()) {
            setRotation(getRotation() - 5);
            randomMove(speed, 50);
        }
    }


    public int getSpeed() {
        return 5;
    }

    @Override
    public void react(MovingEntity movingEntity) {
        if (this.isAktivated) {
            List<MovingEntity> movingEntities = getIntersectingObjects(MovingEntity.class); //if Player touches witch he gets hurt
            for (MovingEntity entity : movingEntities) {
                if (entity instanceof Player) {
                    Player.getInstance().applyDamage(damage);
                    removeFromTerrain();
                    this.isAktivated = false;
                    timers.add(new Timer(this, TriggerEvent.READY_FOR_ATTACK, movingEntity));
                }
            }
        }
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

public abstract class StaticEntity extends Entity {

    public StaticEntity(String fileName) {
        super(fileName);
    }
}

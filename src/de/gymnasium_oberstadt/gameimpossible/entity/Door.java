package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Usable;
import de.gymnasium_oberstadt.gameimpossible.quest.Quest;
import de.gymnasium_oberstadt.gameimpossible.terrain.*;

import java.util.List;

public class Door extends StaticBlockingHandler implements Usable {

    private boolean isClosed;

    private Terrain followingTerrain;

    private boolean locked;

    private Quest quest;

    public Door() {
        super(Player.getInstance().getTerrain().getClosedDoorFileName());
        this.isClosed = true;
    }

    public Door(Terrain followingTerrain, boolean locked, Quest quest) {
        this(followingTerrain, locked);
        this.quest = quest;
    }

    public Door(Terrain followingTerrain, boolean locked) {
        this();
        this.followingTerrain = followingTerrain;
        if (locked) {
            setImage(Player.getInstance().getTerrain().getLockedDoorFileName());
        }
        this.locked = locked;
    }

    @Override
    public void use(Player player) { //method is called to see to use a normal door and to see if it is a portal door
        List<StaticEntity> staticEntities = getTerrain().getObjectsAt(getX(), getY(), StaticEntity.class);
        staticEntities.remove(this);
        if (staticEntities.isEmpty()) {
            if (!isClosed && !locked && followingTerrain != null && (this.quest == null || this.quest.isDone())) { //if the door is a portal and it is already unlocked and open (and there's either no quest bound to it or it's done)
                usePortal();
            } else if (isClosed && !locked) { //if the door is either portal or not and it is closed and unlocked (already used the key in f.e world before)
                this.isClosed = false; //without you cannot enter first door, I don't know why it is like this
                setBlocking(false);
                setImage(Player.getInstance().getTerrain().getOpenDoorFileName());
            } else if (followingTerrain == null && !isClosed) { //if it is a normal door without being a portal and it is open and you want to open it
                isClosed = true;
                //setBlocking(true);
                setImage(Player.getInstance().getTerrain().getClosedDoorFileName());
            }
        }
    }

    public Quest getQuest() {
        return quest;
    }

    public Terrain getFollowingTerrain() {
        return followingTerrain;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void usePortal() {
        // wants to enter another Terrain
        if (Player.getInstance().getTerrain() instanceof OpeningTerrain) {
            this.setImage(Player.getInstance().getTerrain().getOpenDoorFileName());
            locked = false;

            setBlocking(false);
            Player.getInstance().getTerrain().clearTerrain(); //that is important to prevent two images above each other
            Terrain.setTerrain(followingTerrain);
            this.followingTerrain.loadLevel1();
        } else
            // wants to go back to OpeningTerrain (has completed a Terrain)
            if (followingTerrain instanceof OpeningTerrain) {
                this.setImage(Player.getInstance().getTerrain().getOpenDoorFileName());

                Terrain terrain = Player.getInstance().getTerrain();
                Player.getInstance().getTerrain().clearTerrain(); //that is important to prevent two images above each other
                Terrain.setTerrain(followingTerrain);
                followingTerrain.loadLevel1();
                Terrain nextTerrain = terrain.getNextTerrain();
                if (nextTerrain == null) {
                    return;
                }
                List<Door> doors = followingTerrain.getObjects(Door.class);
                for (Door chosenDoor : doors) {
                    Terrain chosenTerrain = chosenDoor.getFollowingTerrain();
                    if (chosenTerrain == nextTerrain || chosenTerrain == terrain || chosenTerrain == ForestTerrain.getInstance()) { //exception: ForestTerrain must be compared here otherwise it will get locked after HellTerrain
                        chosenDoor.setImage(Player.getInstance().getTerrain().getClosedDoorFileName());
                        chosenDoor.setLocked(false);
                        chosenDoor.setBlocking(false);
                    }
                }
            }
        if (followingTerrain instanceof BossTerrain) {
            Terrain.setTerrain(followingTerrain);
            followingTerrain.loadLevel1();
        }
    }
}
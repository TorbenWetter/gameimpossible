package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.*;

import java.util.LinkedList;
import java.util.List;

public abstract class MovingEntity extends Entity implements Triggerable {

    private boolean movementBlocked;

    private float life;

    private List<Timer> timers = new LinkedList<>();

    public MovingEntity(String fileName) {
        super(fileName);

        this.movementBlocked = false;
    }

    /*
     * all sub-classes extending MovingActor HAVE TO call super.act() in their act() method!!!!!
     * this is because otherwise this act() method isn't called because the MovingActor itself is not added to the World
     * also: if sub-class is abstract too, super.act() has to be called in the sub-sub-class too....
     * lol i cant program object oriented (ps. im torben)
     */
    @Override
    public void act() {
        super.act();
        react();
        if (!isAlive()) {
            removeFromTerrain();
        }

        timers.removeIf(timer -> !timer.isRunning());
        for (Timer timer : timers) {
            timer.update();
        }
    }

    public void react() {
        List<Entity> nearEntities = getIntersectingObjects(Entity.class);
        for (Entity entity : nearEntities) {
            if (entity instanceof Reacting) {
                ((Reacting) entity).react(this);
                break;
            }
        }
    }

    public void setLife(float life) {
        this.life = life;
    }

    public float getLife() {
        return life;
    }

    public boolean isAlive() {
        return this.life > 0;
    }

    public void applyDamage(float damage) {
        if (damage > getLife()) {
            setLife(0);
            return;
        }
        setLife(getLife() - damage);

        getImage().setTransparency(128);
        timers.add(new Timer(this, TriggerEvent.SET_VISIBLE, this));
    }

    // if child-classes implement Triggerable too, the SET.VISIBLE check has to be done in it too
    // this is because the "lowest" implementation of a method is executed [in this case of trigger()]
    @Override
    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.SET_VISIBLE) {
            getImage().setTransparency(255);
        }
    }

    public boolean canMove() {
        return !this.movementBlocked && !willIntersectBlocking() && !willBeOutOfWorld();
    }

    public int getSpeed() {
        // standard
        return 7;
    }

    public boolean willBeOutOfWorld() {
        int x = getX(), y = getY();
        super.move(1);
        int newX = getX(), newY = getY();
        setLocation(x, y);

        int wWidth = getTerrain().getWidth(), wHeight = getTerrain().getHeight();
        int pWidth = getImage().getWidth(), pHeight = getImage().getHeight();
        return newX - (pWidth / 2) < 0 || newY - (pHeight / 2) < 0 || newX + (pWidth / 2) >= wWidth || newY + (pHeight / 2) >= wHeight;
    }

    public boolean willIntersectBlocking() {
        int x = getX(), y = getY();
        super.move(1);
        for (Entity entity : getIntersectingObjects(Entity.class)) {
            // either entity implements interface Blocking
            // or entity inherits class StaticBlockingHandler AND has attribute isBlocking == true
            if (entity instanceof Blocking || (entity instanceof StaticBlockingHandler && ((StaticBlockingHandler) entity).isBlocking())) {
                setLocation(x, y);
                return true;
            }
        }
        setLocation(x, y);
        return false;
    }

    public void blockMovement() {
        this.movementBlocked = true;
    }

    public void allowMovement() {
        this.movementBlocked = false;
    }

    public void move(int distance) {
        // test if MovingEntity can move every single time, so at least a few moves can be done (instead of none if distance was too much)
        for (int i = 0; i < distance; i++) {
            if (!canMove()) {
                break;
            }
            super.move(1);
        }
    }
}

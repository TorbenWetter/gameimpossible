package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Jewel extends StaticEntity implements Pickable {
    public Jewel() {
        super(Player.getInstance().getTerrain().getJewelFileName());
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }

}

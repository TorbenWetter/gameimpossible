package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.quest.QuestType;

import java.util.List;

public class Arrow extends StaticEntity {

    private MovingEntity shooter;
    private float damage;

    private float speed = 25f;
    private float acceleration = -7f;

    public Arrow(MovingEntity shooter, float damage) {
        super("Arrow.png");
        this.shooter = shooter;
        this.damage = damage;
    }

    // boolean returns whether the Arrow will be kept in the Terrain
    public boolean update() {
        speed = Math.max(speed + acceleration / 30f, 0f);
        if (speed == 0) {
            return true;
        }

        move((int) speed);
        int imageSize = getImage().getHeight();
        float tolerance = speed - ((float) imageSize / 4);
        if (getX() - tolerance <= 0 || getX() + tolerance >= getTerrain().getWidth() || getY() - tolerance <= 0 || getY() + tolerance >= getTerrain().getHeight()) {
            return false;
        }

        List<MovingEntity> entities = getIntersectingObjects(MovingEntity.class);
        entities.removeIf(entity -> entity == this.shooter);
        for (MovingEntity entity : entities) {
            entity.applyDamage(this.damage);
            if (entity.getLife() <= 0) { // killed
                Player.getInstance().getQuestManager().triggerEvent(QuestType.KILL, entity);
            }
        }
        // Arrow hasn't hit anyone
        return entities.size() == 0;
    }
}

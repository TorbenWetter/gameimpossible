package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Destroyable;

public class Tree extends StaticEntity implements Destroyable {

    public Tree() {
        super(Player.getInstance().getTerrain().getTreeFileName());
    }

    @Override
    public void destroy(Player p) {
        getTerrain().addObject(new Wood(), getX(), getY());
        removeFromTerrain();
    }
}
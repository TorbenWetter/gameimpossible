package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Pickable;
import de.gymnasium_oberstadt.gameimpossible.property.Usable;

import java.util.List;

public class Potion extends StaticEntity implements Usable, Pickable {

    public Potion() {
        super(Player.getInstance().getTerrain().getPotionFileName());
    }

    @Override
    public void use(Player player) {
        float life = player.getLife();
        float maxLife = player.getTerrain().getMaxPlayerLife();
        List<StaticEntity> potionList = player.getObjectsInFront(StaticEntity.class, 0);
        for (StaticEntity staticEntity : potionList) {
            if (staticEntity instanceof Potion) {
                staticEntity.removeFromTerrain();
            }
        }
        if (potionList.isEmpty()) {
            if (player.getSelectedItem() instanceof Potion) {
                player.setSelectedItem(null); //otherwise it is the selected item, which has to be null after usage
            }
        }

        if (life + 5 > maxLife) {
            player.setLife(maxLife);
        } else {
            life = life + 5;
            player.setLife(life);
        }
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }
}

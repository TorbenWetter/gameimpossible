package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Equipable;
import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Shovel extends Weapon implements Pickable, Equipable {

    public Shovel(Material material) {
        super(Player.getInstance().getTerrain().getShovelFileName(), material);
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }
}

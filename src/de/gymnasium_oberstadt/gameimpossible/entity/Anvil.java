package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Blocking;

public class Anvil extends StaticEntity implements Blocking {

    public Anvil() {
        super(Player.getInstance().getTerrain().getAnvilFileName());
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Destroyable;
import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Carrot extends StaticEntity implements Pickable, Destroyable {
    public Carrot() {
        super(Player.getInstance().getTerrain().getCarrotFileName());
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }

    @Override
    public void destroy(Player p) {
        removeFromTerrain();
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Equipable;
import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Bow extends Weapon implements Pickable, Equipable {

    private MovingEntity shooter;

    public Bow(Material material, MovingEntity shooter) {
        super(Player.getInstance().getTerrain().getBowFileName(), material);
        this.shooter = shooter;
    }

    public void shoot(int x, int y, int rotation, float damage) {
        Arrow arrow = new Arrow(this.shooter, damage);
        arrow.setRotation(rotation);
        getTerrain().addObject(arrow, x, y);
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }
}
package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Destroyable;
import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Wood extends StaticEntity implements Pickable, Destroyable {
    public Wood() {
        super(Player.getInstance().getTerrain().getWoodFileName());
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }

    @Override
    public void destroy(Player p) {
        removeFromTerrain();
    }
}

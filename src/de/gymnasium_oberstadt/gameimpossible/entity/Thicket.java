package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Reacting;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;

import java.util.LinkedList;
import java.util.List;

public class Thicket extends StaticEntity implements Reacting, Triggerable {

    private boolean isActive;
    private List<Timer> timers = new LinkedList<>();
    private int damage;

    public Thicket() {
        super(Player.getInstance().getTerrain().getThicketFileName());
        this.isActive = true;
        this.damage = 1;
    }

    @Override
    public void react(MovingEntity movingEntity) {
        if (this.isActive) {
            this.isActive = false;

            timers.add(new Timer(this, TriggerEvent.DEACTIVATE, movingEntity)); //just one timer with only the TriggerEvent DEACTIVE, because there is no trapping, just a short time of stop

            movingEntity.applyDamage(damage);
        }
    }

    @Override
    public void act() {
        super.act();
        timers.removeIf(timer -> !timer.isRunning());
        for (Timer timer : timers) {
            timer.update();
        }
    }

    @Override
    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.DEACTIVATE) {
            this.isActive = true;
        }
    }
}

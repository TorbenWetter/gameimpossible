package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Pickable;
import de.gymnasium_oberstadt.gameimpossible.property.Usable;

import java.util.List;

public class Key extends StaticEntity implements Pickable, Usable {
    //every Key has a door for which one he can get used
    private Door door;

    public Key(Door door) {
        super(Player.getInstance().getTerrain().getKeyFileName());
        this.door = door;
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }

    @Override
    public void use(Player player) {
        if (player.getSelectedItem() instanceof Key) {
            List<StaticEntity> staticEntityList = player.getObjectsInFront(StaticEntity.class);
            for (StaticEntity item : staticEntityList) {
                if (item instanceof Door) {
                    if (item == this.door) {
                        if (door.getQuest() == null || door.getQuest().isDone()) {
                            player.setSelectedItem(null);
                            door.usePortal();
                        }
                    }
                }
            }
        }
    }
}

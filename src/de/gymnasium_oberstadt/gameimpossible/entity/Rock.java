package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Blocking;
import de.gymnasium_oberstadt.gameimpossible.property.Destroyable;

public class Rock extends StaticEntity implements Blocking, Destroyable{

    public Rock() {
        super(Player.getInstance().getTerrain().getRockFileName());
    }

    @Override
    public void destroy(Player p) {
        removeFromTerrain();
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Reacting;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;
import de.gymnasium_oberstadt.gameimpossible.terrain.BossTerrain;
import greenfoot.World;

import java.util.LinkedList;
import java.util.List;

public class Wolf extends Enemy implements Reacting, Triggerable {
    private boolean isActive;
    private List<Timer> timers = new LinkedList<>();
    private float damage;
    private int speed;

    public Wolf() {
        super(Player.getInstance().getTerrain().getWolfFileName());
        scaleImage(100, Player.getInstance().getTerrain().getWolfFileName());
        this.isActive = true;
        damage = getDamage();
        speed = getSpeed();
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        float maxLife = Player.getInstance().getTerrain().getMaxWolfLife();
        setLife(maxLife);
    }

    @Override
    public void act() {
        super.act();
        timers.removeIf(timer -> !timer.isRunning());
        for (Timer timer : timers) {
            timer.update();
        }

        if (isInWorld()) {
            randomMove(speed, 10);
            followMove(Player.getInstance(), 180, speed, 50);
        }
        if (getTerrain() instanceof BossTerrain) {
            damage = 3;
            speed = 3;
        }
    }

    @Override
    public float getDamage() {
        return 1.5f;
    }

    @Override
    public void react(MovingEntity movingEntity) {
        if (this.isActive) {
            this.isActive = false;
            movingEntity.applyDamage(damage);
            timers.add(new Timer(this, TriggerEvent.READY_FOR_ATTACK, movingEntity));
        }
    }

    @Override
    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.READY_FOR_ATTACK) {
            this.isActive = true;
        } else if (triggerEvent == TriggerEvent.SET_VISIBLE) {
            getImage().setTransparency(255);
        }
    }

    @Override
    public int getSpeed() {
        return 2;
    }
}

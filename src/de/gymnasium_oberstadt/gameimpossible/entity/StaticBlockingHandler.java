package de.gymnasium_oberstadt.gameimpossible.entity;

public class StaticBlockingHandler extends StaticEntity {

    private boolean blocking;

    public StaticBlockingHandler(String fileName) {
        super(fileName);
        blocking = true;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }

    public boolean isBlocking() {
        return blocking;
    }
}

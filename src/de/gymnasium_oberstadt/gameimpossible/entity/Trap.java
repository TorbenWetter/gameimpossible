package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Reacting;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;

import java.util.LinkedList;
import java.util.List;

public class Trap extends StaticEntity implements Reacting, Triggerable {

    private List<Timer> timers = new LinkedList<>();

    private boolean isActive;

    private int damage;

    public Trap() {
        super(Player.getInstance().getTerrain().getTrapFileName());
        this.isActive = true;
        this.damage = 2;
    }

    @Override
    public void react(MovingEntity movingEntity) {
        if (this.isActive) {
            this.isActive = false;

            timers.add(new Timer(this, TriggerEvent.RELEASE, movingEntity));
            timers.add(new Timer(this, TriggerEvent.REACTIVATE, movingEntity));

            movingEntity.setLocation(getX(), getY());
            movingEntity.blockMovement();

            movingEntity.applyDamage(this.damage);
        }
    }

    @Override
    public void act() {
        super.act();
        timers.removeIf(timer -> !timer.isRunning());
        for (Timer timer : timers) {
            timer.update();
        }
    }

    @Override
    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.RELEASE) {
            movingEntity.allowMovement();
        } else if (triggerEvent == TriggerEvent.REACTIVATE) {
            this.isActive = true;
        }
    }
}

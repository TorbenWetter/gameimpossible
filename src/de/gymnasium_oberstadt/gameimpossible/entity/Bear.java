package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Reacting;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;
import greenfoot.World;

import java.util.LinkedList;
import java.util.List;

public class Bear extends Enemy implements Reacting, Triggerable {

    private boolean isActive;
    private List<Timer> timers = new LinkedList<>();

    public Bear() {
        super(Player.getInstance().getTerrain().getBearFileName());
        this.isActive = true;
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        float maxLife = Player.getInstance().getTerrain().getMaxBearLife();
        setLife(maxLife);
    }

    @Override
    public void act() {
        super.act();
        timers.removeIf(timer -> !timer.isRunning());
        for (Timer timer : timers) {
            timer.update();
        }

        if (isInWorld()) {
            randomMove(getSpeed(), 10);
            followMove(Player.getInstance(), 180, getSpeed(), 50);
        }
    }

    @Override
    public float getDamage() {
        return 0.75f;
    }

    @Override
    public void react(MovingEntity movingEntity) {
        if (this.isActive) {
            this.isActive = false;
            movingEntity.applyDamage(getDamage());
            timers.add(new Timer(this, TriggerEvent.READY_FOR_ATTACK, movingEntity));
        }
    }

    @Override
    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.READY_FOR_ATTACK) {
            this.isActive = true;
        } else if (triggerEvent == TriggerEvent.SET_VISIBLE) {
            getImage().setTransparency(255);
        }
    }

    @Override
    public int getSpeed() {
        return 1;
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Timer;
import de.gymnasium_oberstadt.gameimpossible.helper.TriggerEvent;
import de.gymnasium_oberstadt.gameimpossible.property.Triggerable;
import de.gymnasium_oberstadt.gameimpossible.property.Usable;

import java.util.LinkedList;
import java.util.List;

public class Cake extends StaticEntity implements Usable, Triggerable { //the cake is to heavy to transport
//it has the ability to give the player two more lifepoints, but you also lose some speed a short time
    private List<Timer> timers = new LinkedList<>();

    public Cake() {
        super(Player.getInstance().getTerrain().getCakeFileName());
    }

    @Override
    public void act() {
        super.act();
        timers.removeIf(timer -> !timer.isRunning());
        for (Timer timer : timers) {
            timer.update();
        }
    }

    @Override
    public void use(Player player) {
        player.setSpeedPlayer(player.getSpeed() - 5); //makes the player slower
        if(player.getLife()+2 <= player.getTerrain().getMaxPlayerLife()) {
            player.setLife(player.getLife() + 2);
        } else{
            player.setLife(player.getTerrain().getMaxPlayerLife());
        }
        timers.add(new Timer(this, TriggerEvent.STOP_ACCELERATION, player));
    }

    @Override
    public void trigger(MovingEntity movingEntity, TriggerEvent triggerEvent) {
        if (triggerEvent == TriggerEvent.STOP_ACCELERATION) {
            Player.getInstance().setSpeedPlayer(movingEntity.getSpeed());
            System.out.println("lo");
            removeFromTerrain();
            if (Player.getInstance().getSelectedItem() instanceof Cake) {
                Player.getInstance().setSelectedItem(null); //it can be in inventory too
            }
        }
    }
}
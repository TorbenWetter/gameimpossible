package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.CraftRecipe;
import de.gymnasium_oberstadt.gameimpossible.helper.Direction;
import de.gymnasium_oberstadt.gameimpossible.property.*;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestManager;
import de.gymnasium_oberstadt.gameimpossible.quest.QuestType;
import de.gymnasium_oberstadt.gameimpossible.terrain.*;
import de.gymnasium_oberstadt.gameimpossible.tool.Inventory;
import de.gymnasium_oberstadt.gameimpossible.tool.LifeBar;
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.World;

import java.util.LinkedList;
import java.util.List;

public class Player extends MovingEntity {

    private final int INV_CAPACITY = 9;

    private LifeBar lifeBar;

    private Inventory inventory;
    private Entity[] inventoryItems = new Entity[INV_CAPACITY];
    private Entity equipedItem = null;

    private QuestManager questManager;

    private static Player instance = null;

    private int speedPlayer; //there are things like the cake, which can makes the player slower


    // design pattern Singleton
    private Player() {
        super("Player.png");
        // private to prevent instantiation
        speedPlayer = getSpeed();
    }

    public static Player getInstance() {
        if (instance == null) {
            instance = new Player();
        }
        return instance;
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);

        float maxLife = getTerrain().getMaxPlayerLife();
        setLife(maxLife);
        this.lifeBar = new LifeBar(maxLife);
        if (this.getTerrain() != OpeningTerrain.getInstance()) {
            world.addObject(this.lifeBar, Math.round(maxLife * Terrain.IMAGE_SIZE / 2), world.getHeight() - Terrain.IMAGE_SIZE);
        }
        this.inventory = new Inventory(INV_CAPACITY);
        world.addObject(this.inventory, INV_CAPACITY * Terrain.IMAGE_SIZE / 2, world.getHeight() - (2 * Terrain.IMAGE_SIZE));
    }

    public void act() {
        super.act();
        if (!isAlive()) {
            StartTerrain.stopMusic();
            System.out.println("Game Over!");
        }

        this.lifeBar.update(getLife());
        this.inventory.setLinkedItems(this.inventoryItems);

        lifeBar.getImage().drawString(Integer.toString(Math.round(Player.getInstance().getLife())), 10, 20);

        if (isInWorld()) {
            performMovement();
            performHotkey();
        }
    }

    public void setQuestManager(QuestManager questManager) {
        this.questManager = questManager;
    }

    public QuestManager getQuestManager() {
        return questManager;
    }

    public void useItem() {
        List<StaticEntity> nearStaticEntities = getObjectsInFront(StaticEntity.class);
        nearStaticEntities.addAll(getIntersectingObjects(StaticEntity.class)); // joins list of objects in front with list of intersecting objects
        for (StaticEntity entity : nearStaticEntities) {
            if (entity instanceof Usable) {
                ((Usable) entity).use(this);
                break;
            }
        }
        //you can use the selectedItem in Inventory if it is Usable
        if (getSelectedItem() != null) {
            if (getSelectedItem() instanceof Usable) {
                ((Usable) getSelectedItem()).use(this); //everything which implements Usable has to care itself about removing itself (z.B. player.setSelectedItem(null))
            }
        }
    }

    public void pickItem() {
        List<StaticEntity> nearStaticEntities = getIntersectingObjects(StaticEntity.class);
        for (StaticEntity entity : nearStaticEntities) {
            if (entity instanceof Pickable) {
                boolean pickedUp = false;
                for (int i = 0; i < inventoryItems.length; i++) {
                    if (inventoryItems[i] == null) {
                        inventoryItems[i] = entity;
                        Pickable item = (Pickable) entity;
                        item.pick(this);
                        this.questManager.triggerEvent(QuestType.COLLECT, item);
                        pickedUp = true;
                        break;
                    }
                }
                if (pickedUp) {
                    break;
                }
            }
        }
    }

    public void craftItem() {
        List<StaticEntity> items = getIntersectingObjects(StaticEntity.class);
        // loop through recipes
        for (CraftRecipe recipe : getTerrain().getRecipes()) {
            // save items that are removed to check if recipe is possible to craft
            List<StaticEntity> removedItems = new LinkedList<>();
            // will become false if one part of the recipe is not given
            boolean containsAll = true;
            // loop through all needed class names
            for (String neededClassName : recipe.getClassNames()) {
                // set to true if one of the items has the same class name
                boolean contains = false;
                for (StaticEntity item : items) {
                    String className = item.getClass().getSimpleName();
                    if (className.equals(neededClassName)) {
                        contains = true;
                        // item with same class name is removed from items list (to make sure there are really 2 items if 2 of the same class are needed)
                        removedItems.add(item);
                        items.remove(item);
                        break;
                    }
                }
                // needed items for recipe are not given
                if (!contains) {
                    containsAll = false;
                    break;
                }
            }

            // if recipe can't be crafted, "reset" lists and go to the next one
            if (!containsAll) {
                items.addAll(removedItems);
                removedItems.clear();
                continue;
            }

            // if recipe CAN be crafted, remove "from" items and add "to" items to Terrain
            for (StaticEntity toRemove : removedItems) {
                toRemove.removeFromTerrain();
            }
            for (Entity entity : recipe.getEntities()) {
                getTerrain().addObject(entity, getX(), getY());
            }
        }
    }

    public void setEquipImage(GreenfootImage itemImage) {
        int imageSize = Terrain.EQUIP_IMAGE_SIZE;
        itemImage.scale(imageSize, imageSize);
        // set in the bottom-right, for that turn the player to the right for a short time
        Direction directionBefore = getDirection();
        turn(Direction.RIGHT);
        getImage().drawImage(itemImage, getImage().getWidth() - imageSize, getImage().getHeight() - imageSize);
        turn(directionBefore);
    }

    public Entity getSelectedItem() {
        // if index is invalid
        int selected = this.inventory.getSelected();
        if (selected < 0 || selected >= this.inventoryItems.length) {
            return null;
        }

        return this.inventoryItems[selected];
    }

    public void setSpeedPlayer(int speedPlayer) {
        this.speedPlayer = speedPlayer;
    }

    public Entity[] getInventoryItems() {
        return inventoryItems;
    }

    public Entity getEquipedItem() {
        return equipedItem;
    }

    public void setSelectedItem(Entity item) {
        // if index is invalid
        int selected = this.inventory.getSelected();
        if (selected < 0 || selected >= this.inventoryItems.length) {
            return;
        }

        this.inventoryItems[selected] = item;
    }

    public void equipItem() {
        Entity selectedItem = getSelectedItem();
        // slot is not empty
        if (selectedItem != null) {
            // slot is equipable (doesn't work if not -> "unequipment" has to be done having selected an empty slot)
            if (selectedItem instanceof Equipable) {
                // equiped is not empty -> swap items
                if (equipedItem != null) {
                    //TODO: Paint two picture above each other
                    resetImage();
                    setEquipImage(selectedItem.getImage());
                    Entity temp = equipedItem;
                    equipedItem = selectedItem;
                    setSelectedItem(temp);
                } else { // equiped is empty -> set equiped
                    setEquipImage(selectedItem.getImage());
                    equipedItem = selectedItem;
                    setSelectedItem(null);
                }
            }
        } else if (equipedItem != null) { // slot is empty AND equiped is not empty -> set slot
            // TODO: doesn't work for the second time
            resetImage();
            setSelectedItem(equipedItem);
            equipedItem = null;
        }
    }

    public void putItem() {
        Entity selectedItem = getSelectedItem();
        if (selectedItem != null) {
            getTerrain().addObject(selectedItem, getX(), getY());
            setSelectedItem(null);
        }
    }

    private void attack() {
        if (this.equipedItem != null) {
            if (this.equipedItem instanceof Equipable && this.equipedItem instanceof Weapon) {
                Weapon weapon = (Weapon) this.equipedItem;
                Material material = weapon.getMaterial();
                float damage = material.getDamage();
                if (weapon instanceof Sword) {
                    attackInFront(damage);
                } else if (weapon instanceof Bow) {
                    Bow bow = (Bow) weapon;
                    bow.shoot(getX(), getY(), getRotation(), damage);
                }
            }
        }
    }

    private void attackInFront(float damage) {
        List<MovingEntity> entities = getObjectsInFront(MovingEntity.class, Terrain.IMAGE_SIZE);
        entities.addAll(getObjectsInFront(MovingEntity.class, Math.round(Terrain.IMAGE_SIZE * 1.5f)));
        entities.addAll(getObjectsInFront(MovingEntity.class, Terrain.IMAGE_SIZE * 2));
        for (MovingEntity entity : entities) {
            entity.applyDamage(damage);
            if (entity.getLife() <= 0) { // killed
                this.questManager.triggerEvent(QuestType.KILL, entity);
            }
        }
    }

    public void destroyObjectInFront() {
        List<Entity> objs = getObjectsInFront(Entity.class);
        if (objs.size() > 0) {
            for (Entity obj : objs) {
                if (obj instanceof StaticEntity) {
                    StaticEntity current = (StaticEntity) obj;
                    if (current instanceof Destroyable) {
                        Destroyable destroyable = (Destroyable) current;
                        destroyable.destroy(this);
                        break;
                    }
                }
            }
        }
    }

    private void performMovement() {
        // isKeyDown() accepts upper case letters
        if (Greenfoot.isKeyDown("W")) {
            turnUp();
            move(speedPlayer);
        }
        if (Greenfoot.isKeyDown("S")) {
            turnDown();
            move(speedPlayer);
        }
        if (Greenfoot.isKeyDown("A")) {
            turnLeft();
            move(speedPlayer);
        }
        if (Greenfoot.isKeyDown("D")) {
            turnRight();
            move(speedPlayer);
        }
    }

    private void performHotkey() {
        // getKey() returns lower case letters
        String key = Greenfoot.getKey();
        if (key != null) {
            if (key.equals("e")) {
                pickItem();
            }
            if (key.equals("q")) {
                putItem();
            }
            if (key.equals("h")) {
                equipItem();
            }
            if (key.equals("f")) {
                destroyObjectInFront();
            }
            if (key.equals("space")) {  //you have to put two items above each other before crafting
                craftItem();
            }
            if (key.equals("u")) {
                useItem();
            }
            if (key.equals("o")) {
                attack();
            }
            // limit to 9 because it's the biggest digit
            for (int i = 0; i < Math.min(inventoryItems.length, 9); i++) {
                if (key.equals(String.valueOf(i + 1))) {
                    this.inventory.setSelected(i);
                }
            }
        }
    }
}

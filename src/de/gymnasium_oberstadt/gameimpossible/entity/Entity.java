package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.terrain.Terrain;
import greenfoot.Actor;
import greenfoot.GreenfootImage;
import de.gymnasium_oberstadt.gameimpossible.helper.*;
import greenfoot.World;

import java.util.List;

// Entity replaces Actor to be able to add functions to both, moving and static "Actor" (entity)
public abstract class Entity extends Actor {

    private GreenfootImage defaultImage;

    private Terrain terrain;

    private boolean isInWorld = false;

    // default constructor is private, so fileName has to be given
    private Entity() {
    }

    public Entity(String fileName) {
        if (fileName != null) {
            setImage(fileName);
            defaultImage = new GreenfootImage(getImage());
        }
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);

        // just in case the World is not a Terrain too
        if (!(world instanceof Terrain)) {
            return;
        }
        this.terrain = (Terrain) world;

        this.isInWorld = true;
    }

    public boolean isInWorld() {
        return isInWorld;
    }

    public void removeFromTerrain() {
        getTerrain().removeObject(this);
        isInWorld = false;
    }

    public float getDamage() {
        // standard value
        return 1.0f;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    @Override
    public void setImage(String fileName) {
        GreenfootImage image = new GreenfootImage(Texts.PATH + "images/entities/" + fileName);
        image.scale(Terrain.IMAGE_SIZE, Terrain.IMAGE_SIZE);
        this.setImage(image);
    }

    public void scaleImage(int size, String fileName) {
        GreenfootImage image = new GreenfootImage(Texts.PATH + "images/entities/" + fileName);
        image.scale(size, size);
        this.setImage(image);
    }

    @Override
    public void setImage(GreenfootImage greenfootImage) {
        Direction currentDirection = getDirection();
        turn(Direction.RIGHT);
        super.setImage(greenfootImage);
        turn(currentDirection);
    }

    public void resetImage() {
        setImage(defaultImage);
    }

    public Direction getDirection() {
        return Direction.fromValue(getRotation());
    }

    /*
     returns the coordinates of the field x (distance) fields away
     null if the direction is not divisible by 90
     values that are not within the World will be "rounded" to valid ones
     e.g. -2 -> 0 or 10 -> 7 if the world's size is only 8
    */
    public Position getPositionAway(int distance) {
        Direction direction = getDirection();
        if (direction == null) {
            return null;
        }

        int x = getX(), y = getY();
        if (direction == Direction.LEFT) {
            x -= distance;
        } else if (direction == Direction.RIGHT) {
            x += distance;
        } else if (direction == Direction.UP) {
            y -= distance;
        } else if (direction == Direction.DOWN) {
            y += distance;
        }
        return new Position(x, y);
    }

    public <A extends Entity> List<A> getObjectsInFront(Class<A> objClass) {
        return getObjectsInFront(objClass, Terrain.IMAGE_SIZE / 2); // Center of Objects is pivotal point
    }

    public <A extends Entity> List<A> getObjectsInFront(Class<A> objClass, int distance) {
        Position position = getPositionAway(distance);
        int x = position.getX(), y = position.getY();

        // ensure coordinates are valid
        int maxX = getTerrain().getWidth() - 1, maxY = getTerrain().getHeight() - 1;
        x = Math.min(Math.max(x, 0), maxX);
        y = Math.min(Math.max(y, 0), maxY);

        return getTerrain().getObjectsAt(x, y, objClass);
    }

    @Override
    public void setRotation(int rotation) {
        if (rotation % 90 == 0) {
            super.setRotation(rotation);
        }
    }

    @Override
    public void turn(int degrees) {
        if (degrees % 90 == 0) {
            super.turn(degrees);
        }
    }

    public void turn(Direction direction) {
        int degrees = direction.getValue() - getRotation();

        if (degrees != 0) {
            turn(degrees);
            getImage().rotate(-degrees);
        }
    }

    public void turnUp() {
        turn(Direction.UP);
    }

    public void turnDown() {
        turn(Direction.DOWN);
    }

    public void turnLeft() {
        turn(Direction.LEFT);
    }

    public void turnRight() {
        turn(Direction.RIGHT);
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.helper.Direction;
import greenfoot.World;

import java.util.Random;

public abstract class Enemy extends MovingEntity {

    public Enemy(String fileName) {
        super(fileName);
    }

    @Override
    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        float maxLife = Player.getInstance().getTerrain().getMaxEnemyLife();
        setLife(maxLife);
    }

    @Override
    public void act() {
        super.act();
    }

    public boolean seeEntity(Entity entity, int radius) {
        if (!getTerrain().getObjects(Entity.class).contains(entity)) {
            return false;
        }
        int xDiff = entity.getX() - getX();
        int yDiff = entity.getY() - getY();
        return Math.abs(xDiff) < radius || Math.abs(yDiff) < radius;
    }

    public void followMove(Entity entity, int radius, int speed, float chance) {
        if (!getTerrain().getObjects(Entity.class).contains(entity)) {
            return;
        }

        int xDiff = entity.getX() - getX();
        int yDiff = entity.getY() - getY();

        Direction moveTo = getDirection();
        boolean moveHorizontally = Math.abs(xDiff) > Math.abs(yDiff);

        // do nothing if diff is larger than allowed radius
        if (moveHorizontally && Math.abs(xDiff) > radius || !moveHorizontally && Math.abs(yDiff) > radius) {
            return;
        }

        if (moveHorizontally) {
            if (xDiff > 0) {
                moveTo = Direction.RIGHT;
            } else if (xDiff < 0) {
                moveTo = Direction.LEFT;
            }
        } else {
            if (yDiff > 0) {
                moveTo = Direction.DOWN;
            } else if (xDiff < 0) {
                moveTo = Direction.UP;
            }
        }

        if (new Random().nextInt((int) (100 / chance)) == 0) {
            turn(moveTo);
        }
        move(speed);
    }

    public void randomMove(int speed, float chance) {
        Direction moveTo = getDirection();
        int rand = new Random().nextInt(4);
        switch (rand) {
            case 0:
                moveTo = Direction.UP;
                break;
            case 1:
                moveTo = Direction.DOWN;
                break;
            case 2:
                moveTo = Direction.LEFT;
                break;
            case 3:
                moveTo = Direction.RIGHT;
                break;
        }

        if (new Random().nextInt((int) (100 / chance)) == 0) {
            turn(moveTo);
        }
        if (isInWorld() && canMove()) {
            move(speed);
        }
    }

    public void moveVertically(int speed) {
        if (!canMove()) {
            turn(Direction.RIGHT);
        }
        if (canMove()) {
            move(speed);
        } else {
            turn(180);
        }
    }

    public abstract float getDamage();

    @Override
    public int getSpeed() {
        return 0;
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

import de.gymnasium_oberstadt.gameimpossible.property.Pickable;

public class Rope extends StaticEntity implements Pickable {

    public Rope() {
        super(Player.getInstance().getTerrain().getRopeFileName());
    }

    @Override
    public void pick(Player player) {
        removeFromTerrain();
    }
}

package de.gymnasium_oberstadt.gameimpossible.entity;

public abstract class Weapon extends StaticEntity {

    private Material material;

    public Weapon(String fileName, Material material) {
        super(fileName);
        this.material = material;
    }

    public Material getMaterial() {
        return material;
    }
}

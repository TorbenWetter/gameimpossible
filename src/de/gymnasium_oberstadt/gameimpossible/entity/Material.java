package de.gymnasium_oberstadt.gameimpossible.entity;

public enum Material {
    FERUM(3.5f, 1.5f), GOLD(1.5f, 2.5f),
    SILVER(2.0f, 2.0f), BRONZE(2.5f, 2.0f),
    STEEL(3.0f, 1.0f), MAGIC(100f, 2.0f);

    private float durability;

    private float damage;

    private Material(float durability, float damage) {
        this.durability = durability;
        this.damage = damage;
    }

    public float getDamage() {
        return damage;
    }

    public float getDurability() {
        return durability;
    }

}
